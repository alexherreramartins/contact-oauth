package br.com.mastertech.contato.service;

import br.com.mastertech.contato.exceptions.ContactsNotFoundException;
import br.com.mastertech.contato.model.Contact;
import br.com.mastertech.contato.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContactService {

    @Autowired
    private ContactRepository contactRepository;

    public Contact create(Contact contact){
        return contactRepository.save(contact);
    }

    public List<Contact> findByOwnerId(int ownerId){

        Optional<List<Contact>> list = contactRepository.findByOwnerId(ownerId);

        if (list.isPresent()){
            return list.get();
        }
        throw new ContactsNotFoundException();
    }

}
