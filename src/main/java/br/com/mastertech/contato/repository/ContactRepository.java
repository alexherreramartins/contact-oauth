package br.com.mastertech.contato.repository;


import br.com.mastertech.contato.model.Contact;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ContactRepository extends CrudRepository<Contact,Long> {

    Optional<List<Contact>> findByOwnerId(int id);
}
