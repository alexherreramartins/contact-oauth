package br.com.mastertech.contato.controller;

import br.com.mastertech.contato.dto.ContactMapper;
import br.com.mastertech.contato.dto.CreateContactRequest;
import br.com.mastertech.contato.model.Contact;
import br.com.mastertech.contato.security.User;
import br.com.mastertech.contato.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contatos")
public class ContactController {

    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactMapper contactMapper;

    @PostMapping
    public Contact createContact(@RequestBody CreateContactRequest createContactRequest,
                                 @AuthenticationPrincipal User user){

        return contactService.create(contactMapper.toContact(createContactRequest,user));
    }

    @GetMapping
    public List<Contact> createContact(@AuthenticationPrincipal User user){
        return contactService.findByOwnerId(user.getId());
    }

}
