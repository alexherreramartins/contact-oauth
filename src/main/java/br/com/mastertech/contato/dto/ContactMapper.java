package br.com.mastertech.contato.dto;


import br.com.mastertech.contato.model.Contact;
import br.com.mastertech.contato.security.User;
import org.springframework.stereotype.Component;

@Component
public class ContactMapper {
    public Contact toContact(CreateContactRequest createContactRequest, User user){
        Contact contact = new Contact();

        contact.setName(createContactRequest.getName());
        contact.setPhoneNumber(createContactRequest.getPhoneNumber());
        contact.setOwnerId(user.getId());

        return contact;
    }
}
